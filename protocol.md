# Silly Text Based File Transfer protocol
Version 1.0

STBFT is intended for sending arbritary files over messaging applications (like IRC) in a text format.  
I mostly made this because i thought it was annoying that IRC DCC SEND leaks client IPs (even though this is not a good solution and you should not use this on IRC. Anything else is funny though. Like discord.)

It offers two modes:  
**Interactive mode**, intended for direct message applications where the two clients can talk in realtime . 
**Broadcast mode**, which provides no on-the-fly configuration, and is more intended for stuff like forums and twitter threads where you could load the data in from an archive.


## Definitions

- `filename`: Any utf-8 string with a length less than 64 characters and must contain no spaces, forward or backward slashes, or control characters. Used as an identifier.
    - It's best to avoid sending files with the same filename in the same session, or at all.  
    This is to avoid the receiver mistaking the wrong message PART sequence than the one intended in case the first PART arrives earlier than the START message
- `part index`: A sequential digit determining the index of the bytes in the sequence.
- `File sender`: The client/server who is broadcasting a file to a `File receiver` or other medium
- `File receiver`: The client who is receiving a file from a `File sender`
- `Interactive mode`: Operation mode where the `File sender` and `File receiver` have a direct two-way method of communication, I.E. an irc conversation.
- `Broadcast mode`: Operation mode where the `File sender` sends its file (usually on a public space) without a targetting specific `File receiver`, or when the `File sender` prefers to not handle negotiation logic, or when a direct two-way method of communication is not possible, I.E. an RSS feed.
    - In Broadcast mode, interactive-only such as `STBFT ASK` commands will not be answered by the `File sender`

## Operating flow

This is the normal order of a STBFT exchange.

| sent by |    command    |  interactive mode only |
|---------|---------------|------------------------|
|sender   | `STBFT QUERY` | Yes                    | 
|receiver | `STBFT OK`    | Yes                    | 
|sender   | `STBFT START` | No                     |
|sender   | `STBFT PART`  | No                     |
|sender   | `STBFT END`   | No                     |
|receiver | `STBFT ASK`   | Yes                    |
|receiver | `STBFT THX`   | Yes                    |
|sender   | `STBFT QUIT`  | Yes                    |

Items where `interactive mode only` are set to `Yes` must be skipped & ignored when using `Broadcast mode`.  
Note that the `File receiver` may also respond with `STBFT NO` instead of `STBFT OK`, effectively ending the exchange immediatly.

## Encodings
List of officially supported binary-to-text encodings

- `base64`

The resulting text is assumed to be encoded in utf-8

## Commands

Note that the prefixed STBFT and command name should be treated case-insensitively, so that the following are all valid:
- STBFT START
- stbft start
- STBFT start
- stbft START
- Stbft stART  

All following parameters should be handled as case-sensitive.

### STBFT QUERY
Asks the File receiver if they accept the transmission of a file.  
Used exclusively in interactive mode.  
`File sender` must wait on `file receiver` to answer with `STBFT OK` or `STBFT NO`, or send a `STBFT QUIT` if a reasonable timeout is exceeded.

Used by: `File sender`

Format:
```
STBFT QUERY <filename> 1 <encoding> <part count> <file size>
```
Parameters:
- filename:   sent file's name
- encoding:   an encoding identifier string
- part count: an integer number that indicates in how many parts the file will be sent
- file size:  index of the size of the file to be sent in bytes

### STBFT START
Marks the start of a file transfer.  

Used by: `File sender`

Format:
```
STBFT START <filename> 1 <mode> <encoding> <part count> <file size>
```
Parameters:
- filename:   sent file's name
- mode:       "broadcast" or "interactive"
- encoding:   an encoding identifier string
- part count: an integer number that indicates in how many parts the file will be sent
- file size:  index of the size of the file to be sent in bytes


### STBFT PART
Contains one part of the actual data of the file.

Used by: `File sender`

Format:
```
STBFT PART <filename> <part index> <data> 
```
Parameters: 
- filename:   sent file's name
- part index: sequential index of the data
- data:       part of the file data in the encoding that the `STBFT START` command indicated


### STBFT END
Indicates that all parts have been sent.  
If in broadcast mode, this will conclude the exchange and nothing further needs to be done.
If in interactive mode, wait until the file receiver sends `STBFT THX`, or after a short timeout where no `STBFT ASK`s are received.
After this, send a `STBFT QUIT`.

Used by: `File sender`

Format:
```
STBFT END <filename>
```
Parameters:
- filename:   sent file's name



### STBFT QUIT
Indicates the file sender will no longer provide this file. After this, both parties should consider the exchange ended.  
Optional in broadcast mode, required in interactive mode.
Should be sent in response to a `STBFT THX`.  
This command may also be used to cancel transmission of a file before all parts have been sent.  

Used by: `File sender`

Format:
```
STBFT QUIT <filename>
```
Parameters:
- filename:   sent file's name


### STBFT OK
Used exclusively in interactive mode.  
Indicates the file receiver accepts the file sender's query. After this, the file sender may start it's START-PART-END sequence.

Used by: `File receiver`

Format:
```
STBFT OK <filename>
```
Parameters:
- filename:   queried file's name


### STBFT NO
Used exclusively in interactive mode.  
Indicates the file receiver does not accept the file sender's query. After this, the both parties should consider the exchange ended.

Used by: `File receiver`

Format:
```
STBFT NO <filename>
```
Parameters:
- filename:   queried file's name


### STBFT THX
Used exclusively in interactive mode.  
Indicates the file receiver has received all parts.
After this, the file sender should send a `STBFT QUIT`, concluding the exchange.

Used by: `File receiver`

Format:
```
STBFT THX <filename>
```
Parameters:
- filename:   received file's name


### STBFT ASK
Used exclusively in interactive mode.  
Requests the file sender to re-send a specific part.  
Should be run after `STBFT END`, but before `STBFT QUIT`.
This may be sent multiple times, but its recommended that file senders implement a limit to prevent infinite request loops.  

Used by: `File receiver`

Format:
```
STBFT ASK <filename> <part index>
```
Parameters:
- filename:   received file's name
- part index: specific part to be re-sent



