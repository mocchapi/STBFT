import base64
import math

# Proof of concept implementation, could for sure be better

def bytes_get_base64_text(data:bytes):
    return base64.b64encode(data).decode('utf-8')

def bytes_get_base64_size(data:bytes):
    return math.ceil(len(data)*1.333333333333)

def text_get_base64_bytes(data:str):
    return base64.b64decode(data)

class STBFTReceiveBroadcast():
    has_started = False
    data = None
    filename = ""
    encoding = ""
    part_count = 0
    size = 0
    received_parts = []
    def __init__(self):
        pass

    def parse_command(self, text:str):
        lowcase = text.lower()
        split = text.split(' ')
        
        if lowcase.startswith('stbft start'):
            if self.has_started:
                return
            # split[0] is stbft
            # split[1] is start
            # split[3] is version (1)
            # Split[4] is mode, which we assume to be broadcast
            print("feed start")
            self.feed_start(split[2], split[5], int(split[6]), int(split[7]))
        elif lowcase.startswith('stbft part'):
            # split[0] is stbft
            # split[1] is part
            if split[2] != self.filename:
                return
            print("feed part")
            self.feed_part(int(split[3]), split[4])
        elif lowcase == f'stbft end {self.filename}':
            if split[2] != self.filename:
                return
            print("feed end")
            self.feed_end()
            

    def feed_start(self, filename:str, encoding='base64', part_count:int=0, file_size:int=0):
        self.has_started = True
        self.data = None
        self.filename = filename
        self.encoding = encoding
        self.part_count = part_count
        self.size = file_size

        self.received_parts = []

    def feed_part(self, index:int, data:str):
        self.received_parts.insert(index, (index, data))

    def feed_end(self):
        if len(self.received_parts) != self.part_count:
            raise Exception(f"Received parts != defined parts! was promised {self.part_count}, but received only {len(self.received_parts)}")
        self.received_parts.sort(key=lambda x: x[0])
        self.data = text_get_base64_bytes(self.received_parts[0][1])
        for item in self.received_parts[1:]:
            self.data += text_get_base64_bytes(item[1])
        return self.data

class STBFTSendBroadcast():
    filename = ""
    encoding = ""
    parts = 0
    size = 0
    size_per_parts = 0
    data = None
    def __init__(self, data:bytes, filename:str, encoding:str="base64", max_message_length:int=256):
        self.filename = filename
        self.encoding = encoding
        self.size = bytes_get_base64_size(data)
        self.data = data
        max_message_length -= len(f'STBFT PART {self.filename} ')
        self.parts = math.ceil(self.size / max_message_length)
        self.size_per_parts = math.ceil(self.size/self.parts)


    def get_full_sequence(self):
        return [self.get_start()] + self.get_all_parts() + [self.get_end()]

    def get_start(self):
        return f"STBFT START {self.filename} 1 broadcast {self.encoding} {self.parts} {self.size}"

    def get_part(self, idx):
        data = self.data[idx*self.size_per_parts : (idx+1)*self.size_per_parts]
        print(f"get part {idx}. slice is [{idx*self.size_per_parts} : {(idx+1)*self.size_per_parts}]: {data}")
        return f"STBFT PART {self.filename} {idx} {bytes_get_base64_text(data)}"

    def get_end(self):
        return f"STBFT END {self.filename}"

    def get_all_parts(self):
        out = []
        for i in range(self.parts):
            out.append(self.get_part(i))
        return out


filename = 'image.png'
with open("test_files/" + filename, 'rb') as f:
    data = f.read()
print(data)
print()
print(bytes_get_base64_text(data))
print(len(bytes_get_base64_text(data)), bytes_get_base64_size(data))
new = STBFTSendBroadcast(data, filename, 'base64')

print()
print('size: ', new.size)
print('parts: ', new.parts)
print('size per parts: ', new.size_per_parts)

print(new.get_all_parts())
with open('test_logs/' + filename + ".txt", 'w') as f:
    f.write('\n'.join(new.get_full_sequence()))

print()
print("start feed")


receiver = STBFTReceiveBroadcast()
for message in new.get_full_sequence():
    print(">> FEED ",message)
    receiver.parse_command(message)

with open('test_output/' + receiver.filename, 'wb') as f:
    f.write(receiver.data)
